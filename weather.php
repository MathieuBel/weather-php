<?php
$config = include('config.php');

$servername = $config["SQL_URL"];
$username = $config["SQL_ID"];
$password = $config["SQL_PASS"];
$db = $config["SQL_TABLE"];
try {
    $conn = new PDO("mysql:host=$servername;dbname=$db", $username, $password);

    $sth = $conn->query('SELECT * FROM history WHERE postal_code="'.$_GET["zipcode"].'"')->fetch();
    if($sth["postal_code"] == NULL){
        $data = json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/weather?zip=".$_GET["zipcode"].",FR&appid=".$config["API_KEY"]."&lang=".$config["LANG"]."&units=".$config["UNIT"].""), true);
        $json = json_encode(array("METHOD" => "FROM_API", "TEMP_NOW" => $data["main"]["temp"], "TEMP_MIN" => $data["main"]["temp_min"], "TEMP_MAX" => $data["main"]["temp_max"], "WEATHER_NOW" => $data["weather"][0]["description"]));
        $data = [
            'postal_code' => $_GET["zipcode"],
            'temp_now' => $data["main"]["temp"],
            'temp_min' => $data["main"]["temp_min"],
            'temp_max' => $data["main"]["temp_max"],
            'weather_now' => $data["weather"][0]["description"],
        ];
        $sql = "INSERT INTO history (postal_code, temp_now, temp_min, temp_max, weather_now) VALUES (:postal_code, :temp_now, :temp_min, :temp_max, :weather_now)";
        $stmt = $conn->prepare($sql);
        $stmt->execute($data);
    }else{
        if(strtotime($sth["used_at"]) > strtotime('+'.$config["TIME_UPDATE"].' minutes',  time())){
            $data = json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/weather?zip=".$_GET["zipcode"].",FR&appid=".$config["API_KEY"]."&lang=".$config["LANG"]."&units=".$config["UNIT"].""), true);
            $json = json_encode(array("METHOD" => "FROM_API_BECAUSE_BDD_EXPIRED", "TEMP_NOW" => $data["main"]["temp"], "TEMP_MIN" => $data["main"]["temp_min"], "TEMP_MAX" => $data["main"]["temp_max"], "WEATHER_NOW" => $data["weather"][0]["description"]));
        
            $req = $conn->prepare("UPDATE history SET postal_code = :postal_code, temp_now = :temp_now, temp_min = :temp_min, temp_max = :temp_max, weather_now = :weather_now, used_at = :used_at WHERE id = '".$sth["id"]."'");
            $req->bindParam(':postal_code', $_GET["zipcode"]);
            $req->bindParam(':temp_now', $data["main"]["temp"]);
            $req->bindParam(':temp_min', $data["main"]["temp_min"]);
            $req->bindParam(':temp_max', $data["main"]["temp_max"]);
            $req->bindParam(':weather_now', $data["weather"][0]["description"]);
            $req->bindParam(':used_at', date("Y-m-d H:i:s"));
            $req->execute();
        }else{
            $json = json_encode(array("METHOD" => "FROM_BDD", "TEMP_NOW" => (float)$sth["temp_now"], "TEMP_MIN" => (float)$sth["temp_min"], "TEMP_MAX" => (float)$sth["temp_max"], "WEATHER_NOW" => $sth["weather_now"]));
        }
    }
    echo $json;
} catch(PDOException $e){
    echo "Connection failed: " . $e->getMessage();
}
?>